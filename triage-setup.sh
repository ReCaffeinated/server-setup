#!/bin/bash
set -e

if ! ping -c 4 1.1.1.1; then
    printf "This script requires an active connection to the internet\n\
    	   Please follow the steps found on \n\
	   https://wiki.archlinux.org/index.php/Wpa_supplicant\n\
	   to connect this system to wifi, or ignore those instructions and use ethernet"
    exit
fi

pacman -Sy pacman-contrib --noconfirm
curl -s "https://www.archlinux.org/mirrorlist/?country=US&country=CA&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 -

timedatectl set-ntp true
pacman -Syy archlinux-keyring --noconfirm

function legacy_install(){
    parted --script -a optimal -- "$disk" \
	   mklabel msdos\
	   mkpart primary 0% -4096MB\
	   mkpart primary -4096MB 100%

    mkfs.ext4 "$disk"1
    mkswap "$disk"2
    swapon "$disk"2

    mount "$disk"1 /mnt

    pacstrap /mnt base linux linux-firmware

    genfstab -U /mnt >> /mnt/etc/fstab
    cp part2.sh grub_legacy.sh /mnt/bin/

    arch-chroot /mnt part2.sh 'legacy' "$disk"

    #    echo "You can reboot now"
}


function uefi_install(){
    parted --script -a optimal -- "$disk" \
	   mklabel gpt\
	   mkpart primary 0% 512MB\
	   mkpart primary 512MiB -4096MB\
	   mkpart primary -4096MB 100%

    mkfs.fat -F 32 "$disk"1
    mkfs.ext4 "$disk"2
    mkswap "$disk"3
    swapon "$disk"3

    mount "$disk"2 /mnt
    mkdir /mnt/efi
    mount "$disk"1 /mnt/efi

    pacstrap /mnt base linux linux-firmware

    genfstab -U /mnt >> /mnt/etc/fstab
    cp part2.sh grub_uefi.sh /mnt/bin/

    arch-chroot /mnt part2.sh 'UEFI'
    

    #echo "You can reboot now"
}

lsblk
echo "Input selection as '/dev/sdx' where x is the corresponding drive letter"
echo "Do not usne partition numbers such as /dev/sdx1, the drives themselves should be targeted"
read -r -p "Please select a disk: " disk

if ls /sys/firmware/efi/efivars; then
    bootmode='UEFI'
    uefi_install
else
    bootmode='legacy'
    legacy_install  
fi


#rm /mnt/bin/part2.sh /mnt/bin/grub_.*sh

reboot now
