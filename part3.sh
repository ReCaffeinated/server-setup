#!/bin/bash

set -e

echo "NOTE: Interfaces must be entered as such \"eth0\" \"enp2s0\" without quotes"
echo "If after reboot systemctl status dnsmasq reports state 'failed' the issue is likely"
echo "the interface described in /etc/dhcpcd.conf.  This section is imperfect and prone to error"
echo "expect complications and be prepared to perform manual configuration"

ip link
read -r -p "Please select the ethernet interface" ethernet
sed -i "s/interface.*/interface $ethernet/" /etc/dhcpcd.conf
firewall-cmd --permanent --zone=trusted --change-interface="$ethernet"

sed -i 's/Wants=.*/Requires=network-online.target\nWants=nss-lookup.target/' /usr/lib/systemd/system/dnsmasq.service


chown -R dnsmasq /mnt/srv/tftp
chgrp -R dnsmasq /mnt/srv/tftp



#if wget http://10.5.5.1/triage-custom.iso -O /srv/triage-custom.iso; then
    #printf "Previous server found, I'm surprised to see anyone using this option\n"
    #sleep 5
    #printf "To be honest with you it means a lot to me that the system is being propogated\n"
    #sleep 5
    #printf "The entire industry needs this technology, and here you are spreading it, thank you\n"
    #sleep 5
    #printf "I put a lot of work into this server setup and it functioned beautifully, so I hope\n\
    #	   it continues to serve you as well as it has us.  Good Luck you crazy geek, whoever\n\
    #you are."
    #sleep 5
    #printf "Signed, Devon Yard, FAMCe Server Technician, triage author, and part-time wizard\n\
    #	   --29 June 2020"
    #sleep 5
#    printf "You're good to go"
    
#    exit
#else
#    echo "You'll need to find triage-custom.iso from a previous install and place it in the /srv/http/ directory on the new build.  Other than that, configurations are done."
#    exit
#fi


