#!/bin/bash

#echo 'KEYMAP=dvorak' > /etc/vconsole.conf

set -e
bootmode="$1"
disk="$2" 

ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
hwclock --systohc

sed -i -e 's;#en_US.UTF-8 UTF-8;en_US.UTF-8 UTF-8;' /etc/locale.gen
locale-gen

pacman -Syu --noconfirm --needed wget dialog nano vim emacs ntp \
       openssh lynx archiso texlive-most dhcpcd cups cups-pdf\
       git base-devel firewalld man-db man-pages texinfo sudo\
       syslinux grub wpa_supplicant dnsmasq efibootmgr darkhttpd \
       #plasma kde-applications packagekit-qt5 

systemctl enable dnsmasq.service darkhttpd.service dhcpcd.service \
    sshd.service ntpd.service cups.service #sddm.service NetworkManager.service

#Dialog generates user input boxes, but then outputs said input on stderr for some reason
#The easiest way to work around this is with a file i/o redirect
#It makes the install process smoother so I'm leaving it in but I DO NOT like this
dialog --inputbox 'Please enter a Hostname' 0 0 triage-server 2> hostname.txt
hostname="$(cat hostname.txt)"

echo "$hostname" > /etc/hostname

echo '127.0.0.1    localhost' > /etc/hosts
echo '::1          localhost' >> /etc/hosts
echo '127.0.1.1    "$hostname".localdomain  "$hostname"' >> /etc/hosts

mkinitcpio -P

#TODO: Add elif for exit value 12 'can't create home directory'
while true; do
    dialog --inputbox 'Please enter a Username' 0 0 technician 2> username.txt
    username="$(cat username.txt)"
    if useradd -m "$username"; then break; fi;
done
cd /home/"$username"/
mkdir Documents Downloads Desktop logs logs/stickets
chown -R "$username" /home/"$username"
echo "Enter password for $username "
while true; do if passwd "$username" ; then break; fi; done
echo "Enter password for root "
while true; do if passwd root ; then break; fi; done
groupadd sudo
usermod -a -G sudo,cups "$username"
printf ' %%sudo\tALL=(ALL) ALL' | EDITOR='tee -a' visudo
#passwd -l

if [[ "$bootmode" == 'UEFI' ]]; then
    grub_uefi.sh
else
    grub_legacy.sh "$disk"
fi

#TODO: automate firewall configurations, until then firewalld is disabled by default


#The following code and  associated function were to automate the download of a vanilla arch install iso for modification
#These wound up being entirely redundant but I worked really hard on them so I'm leaving them in
#mirrorList="$(lynx --dump --listonly https://www.archlinux.org/download/ |\
#    grep torrent | sed 's/.*https/https/' |\
#    lynx --dump --listonly - | sed 's/..:http/\nhttp/g' | grep -a http:// )"

#function downloadISO() {
#    randomMirror="$(echo "$mirrorList" | shuf -n 1 -)"
#    isoLink="$(lynx --dump --listonly "$randomMirror" | grep .iso$ | sed 's/.*http/http/')"
#    if ! wget "$isoLink" --tries=1 --timeout=10 -O /home/"$username"/Downloads/arch.iso; then
#	downloadISO
#    else
#	vanillaIso=/home/"$username"/Downloads/arch.iso
#    fi
#}

#downloadISO

#grab the archiso build files to be customized
mkdir /root/build-directory
cp -r /usr/share/archiso/configs/releng/ /root/build-directory/
cd /root/build-directory/releng
git clone https://gitlab.com/recaffeinated/server-setup.git ../server-setup
cat ../server-setup/pkglist.txt >> packages.x86_64
sort packages.x86_64 >> sortedlist.txt
uniq sortedlist.txt > packages.x86_64
rm sortedlist.txt

cp ../server-setup/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
cp ../server-setup/wpa_supplicant.conf airootfs/root/wpa_supplicant.conf
cp ../server-setup/wireless.sh airootfs/root/wireless.sh
chmod +x airootfs/root/wireless.sh

printf "sleep 4s\n\
	if wget -t 1 -T 1 http://10.5.5.1/triage.tar ; then\n\
	   tar -xf triage.tar\n\
	   ./triage/triage\n\
	else\n\
	   ./wireless.sh\n\
	 fi\n" >> airootfs/root/.automated_script.sh

sed -i -e 's/MENU TITLE Arch Linux/MENU TITLE Arch Linux\nPROMPT 1\nTIMEOUT 1/' syslinux/archiso_head.cfg

#generate ssh keypairs and authorization tokens
mkdir airootfs/root/.ssh
echo "Please enter an empty passphrase, doing otherwise will break automation"
ssh-keygen -f airootfs/root/.ssh/id_rsa
mkdir /home/"$username"/.ssh
cp airootfs/root/.ssh/id_rsa.pub /home/"$username"/.ssh/authorized_keys
ssh-keygen -A
/bin/sshd
ssh-keyscan localhost | sed -e 's/localhost/10.5.5.1/g' >> airootfs/root/.ssh/known_hosts
ssh-keyscan localhost | sed -e 's/localhost/192.168.0.250/g' >> airootfs/root/.ssh/known_hosts

#build the actual iso file
mkarchiso -v ./
mv out/archlinux-*.iso /srv/http/triage-custom.iso
mkdir /srv/tftp
cp -r /usr/lib/syslinux/bios /srv/tftp/
mkdir /srv/tftp/bios/pxelinux.cfg
cp ../server-setup/pxelinux.cfg /srv/tftp/bios/pxelinux.cfg/default

chown -R dnsmasq /srv/tftp
chgrp -R dnsmasq /srv/tftp

#takes the output of 'ip link' and trims it down to the interface names
#capping it off with a reverse filter for the loopback, as it's not needed
echo 'Network interfaces'
ip link | grep ^.:\ | awk '{print $2}' | sed -e 's/://' | grep -v lo
echo "Input device as written above."
read -r -p "Please select an ethernet interface for server bindings: " ethernet
sed -i -e "s/PLACEHOLDER/$ethernet/" ../server-setup/dhcpcd.conf
sed -i -e "s/PLACEHOLDER/$ethernet/" ../server-setup/dnsmasq.conf
cp ../server-setup/dhcpcd.conf /etc/dhcpcd.conf
cp ../server-setup/dnsmasq.conf /etc/dnsmasq.conf
echo 'tos orphan 15' >> /etc/ntpd.conf

#Grab triage propper
git clone https://gitlab.com/recaffeinated/triage.git ../triage
#sed -i -e "s/remoteLogin=.*/remoteLogin='$username@10.5.5.1'/" -e "s/remoteUser=.*/remoteUser='$username'/" ../triage/triage
gcc ../triage/endscreen.c -Wall -lcurses -o ../triage/endscreen
tar -cf /srv/http/triage.tar ../triage

#allow triage clients internet access for timesyncing purposes
iptables -t nat -A POSTROUTING -s 10.5.5.0/24 -j MASQUERADE





exit
