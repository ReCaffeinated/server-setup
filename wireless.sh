#!/bin/bash

ip link
read -r -p "Please select the wireless interface: " wireless
wpa_supplicant -B -i "$wireless" -c wpa_supplicant.conf

while ! ping -c 4 192.168.0.250; do
    echo "Failed to connect to server; Retrying..."
    sleep 4s
done

wget http://192.168.0.250/triage.tar
tar -xf triage.tar
sed -i -e 's/technician@10.5.5.1/technician@192.168.0.250/' \
    -e 's/ntpdate -s 10.5.5.1/ntpdate -s 192.168.0.250/' triage/triage
./triage/triage
