#!/bin/bash
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=Dreadwyrm
grub-mkconfig -o /boot/grub/grub.cfg
