#!/bin/bash
disk="$1"
grub-install --target=i386-pc "$disk"
grub-mkconfig -o /boot/grub/grub.cfg
